
/*	Create database		*/
create database helpdesk;

use helpdesk;

/*	employee table: to store employee information	*/
create table employee
(
	empid_pk	char(10),
	empfname	varchar(50),
	empmname	varchar(50),
	emplname	varchar(50),
	empdesignation	varchar(50),
	empmob		char(15),
	empofcext	char(6),
	empdept_fk	char(10)
);


/*	department table to store department data	*/
create table department
(
	deptid_pk	char(10),
	deptname	varchar(50),
	deptlocation	varchar(50),
	depthead_fk	char(10)
);


/*	complaint table to store department data	*/
create table complaint
(
	cid_pk_ai	int,
	issue		varchar(200),
	cdesc		varchar(500),
	cfiled_date	date,
	cresolved_date	date,
	cstatus		char(15),
	cemp_fk		char(10),
	cdept_fk	char(10)
);



/*
	Note	:	Queries for table creation are as follows but.
			Constraints are not specified yet please_
			Suggest constraints and columns(if needed) for above schema and finalize design at the earliest.
			Update the database.sql file on gitlab if wants to add/rename columns.
	
*/
